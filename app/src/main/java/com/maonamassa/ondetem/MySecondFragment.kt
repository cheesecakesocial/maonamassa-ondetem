package com.maonamassa.ondetem


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.maonamassa.ondetem.R


/**
 * A simple [Fragment] subclass.
 */
class MySecondFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater?.inflate(R.layout.fragment_my_second, null, false)
    }

}// Required empty public constructor
