package com.maonamassa.ondetem.admin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.maonamassa.ondetem.R

class PreLoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pre_login)
    }
}
