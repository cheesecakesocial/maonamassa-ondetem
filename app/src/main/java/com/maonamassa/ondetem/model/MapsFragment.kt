package com.maonamassa.ondetem.model

import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.maonamassa.ondetem.R
import java.util.ArrayList


class MapsFragment : Fragment(), OnMapReadyCallback, LocationListener{

    private var mMap: GoogleMap? = null
    var positionNow: LatLng? = null
    var markerNow: Marker? = null
    var didUpdateLocation = false




    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater?.inflate(R.layout.fragment_maps, null, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        if(Build.VERSION.SDK_INT <23){
            requestLocation()
        }
    }

     fun requestLocation(){
        val service = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER)
         if(enabled) {

            getCurrentLocation()
        }
    }

    private fun getCurrentLocation() {
        val locationManager = activity.getSystemService(LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f,this)

    }

    fun updateList(list: ArrayList<Project>){
        val filteredProjects = ArrayList<Project>()
        for (project in list) {
                if (project.mapLongitude != 0.0) {
                    filteredProjects.add(project)
                    mMap?.addMarker(MarkerOptions().position(LatLng(project.mapLatitude!!, project.mapLongitude!!)).title(project.title))
                }

        }

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

//        val rxPermissions = RxPermissions(this)
//        rxPermissions
//                .request(Manifest.permission.ACCESS_FINE_LOCATION)
//                .subscribe({ granted ->
//                    if (granted) { // Always true pre-M
//                        Toast.makeText(this,"location permission granted", Toast.LENGTH_SHORT).show()
//                        requestLocation()
//
//
//                        // I can control the camera now
//                    } else {
//                        Toast.makeText(this,"location permission dinied", Toast.LENGTH_SHORT).show()
//                        // Oups permission denied
//                    }
//                })

//        val sydney = LatLng(-27.0, -48.0)
//        mMap!!.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
//        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(sydney))
//
//        val floripa = LatLng(-29.0, -50.0)
//        mMap!!.addMarker(MarkerOptions().position(floripa).title("Marker in floripa"))
//        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(floripa))
    }

    private fun updateMapLocation(latLong: LatLng) {
        mMap?.isMyLocationEnabled = true

        if (markerNow != null){
            markerNow?.position = latLong
        }else{

//            markerNow = mMap?.addMarker(MarkerOptions().position(latLong).title("Minha Localização"))

        }
        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLong,17f))
    }

    override fun onLocationChanged(p0: Location?) {



        val latitude = p0?.latitude
        val longitude = p0?.longitude
        if (latitude != null && longitude !=null && !didUpdateLocation) {
            positionNow = LatLng(latitude, longitude)
            updateMapLocation(positionNow ?: return)
            didUpdateLocation = true
        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
    }

    override fun onProviderEnabled(provider: String?) {
    }

    override fun onProviderDisabled(provider: String?) {
    }
}