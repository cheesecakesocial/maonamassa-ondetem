package com.maonamassa.ondetem

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
//import org.junit.experimental.results.ResultMatchers.isSuccessful
import android.support.annotation.NonNull
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_new_user.*


class NewUserActivity : AppCompatActivity() {

    private var userCompany: EditText? = null
    private var userName: EditText? = null

    private var userEmail: EditText? = null
    private var userPassword: EditText? = null
    private var userRePassword: EditText? = null
    private var userPhone: EditText? = null
    private var userCNPJ: EditText? = null
    private var orgCheckbox: CheckBox? = null
    private var orgEmail: EditText? = null
    private var orgPhone: EditText? = null

    var isOrganization = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_user)

        userCompany = findViewById(R.id.editTextCompany) as EditText
        userName = findViewById(R.id.editTextName) as EditText
        userEmail = findViewById(R.id.editTextEmail) as EditText
        userPassword = findViewById(R.id.editTextPassword) as EditText
        userRePassword = findViewById(R.id.editTextRePassword) as EditText
        userPhone = findViewById(R.id.editTextPhone) as EditText
        userCNPJ = findViewById(R.id.editTextCNPJ) as EditText
        orgCheckbox = findViewById(R.id.checkBox) as CheckBox
        orgEmail = findViewById(R.id.editTextEmailCorp) as EditText
        orgPhone = findViewById(R.id.editTextTelefoneCorp) as EditText

        val createButton = findViewById(R.id.createButton) as Button
        createButton.setOnClickListener {
            if (validateData()) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                FirebaseAuth.getInstance()
                        .createUserWithEmailAndPassword(userEmail?.text.toString().trim(), userPassword?.text.toString().trim())
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {

                                val user = FirebaseAuth.getInstance().currentUser

                                val usermap = HashMap<String, Any>()
                                usermap.put("name", userName?.text.toString())
                                usermap.put("phone", userPhone?.text.toString())
                                usermap.put("email", userEmail?.text.toString())

                                if (orgCheckbox?.isChecked ?: false) {
                                    usermap.put("isApproved", false)
                                    usermap.put("isOrganization", true)
                                    usermap.put("CNPJ", userCNPJ?.text.toString())
                                    usermap.put("company", userCompany?.text.toString())
                                    usermap.put("orgEmail", orgEmail?.text.toString())
                                    usermap.put("orgPhone", orgPhone?.text.toString())
                                } else {
                                    usermap.put("isApproved", true)
                                    usermap.put("isOrganization", false)
                                    usermap.put("CNPJ", "")
                                    usermap.put("company", "")
                                    usermap.put("orgEmail", "")
                                    usermap.put("orgPhone", "")
                                }

                                FirebaseDatabase.getInstance().reference.child("users")
                                        .child(user?.uid)
                                        .updateChildren(usermap)
                                        .addOnCompleteListener { task ->

                                            if (task.isSuccessful) {
                                                finish()

                                            } else {
                                                Toast.makeText(application, "Falha no cadastro, tente novamente!", Toast.LENGTH_LONG).show()
                                            }
                                        }
                            } else {
                                Toast.makeText(application, "Falha no cadastro, tente novamente!", Toast.LENGTH_LONG).show()
                            }
                        }

                }
            }
        }

        internal fun validateData(): Boolean {
            if (userName!!.text.toString().isEmpty()) {
                Toast.makeText(this@NewUserActivity, "Digite o seu nome completo", Toast.LENGTH_SHORT).show()
                return false
            }
            if (userEmail!!.text.toString().isEmpty()) {
                Toast.makeText(this@NewUserActivity, "Digite o e-mail", Toast.LENGTH_SHORT).show()
                return false
            }
            if (userPassword!!.text.toString().isEmpty()) {
                Toast.makeText(this@NewUserActivity, "Senha inválida!", Toast.LENGTH_SHORT).show()
                return false
            }
            if (userRePassword!!.text.toString().isEmpty()) {
                Toast.makeText(this@NewUserActivity, "Confirme a sua senha!", Toast.LENGTH_SHORT).show()
                return false
            }
            if (!userRePassword!!.text.toString().isEmpty() && userRePassword!!.text.toString() != userPassword!!.text.toString()) {
                Toast.makeText(this@NewUserActivity, "As senhas estão diferentes!", Toast.LENGTH_SHORT).show()
                return false
            }
            if (userPhone!!.text.toString().isEmpty()) {
                Toast.makeText(this@NewUserActivity, "Digite o Telefone!", Toast.LENGTH_SHORT).show()
                return false
            }

            if (isOrganization && userCompany!!.text.toString().isEmpty()) {
                Toast.makeText(this@NewUserActivity, "Digite o nome da Entidade / Empresa", Toast.LENGTH_SHORT).show()
                return false
            }

            return true
        }

        fun onOrganizerCheckboxClicked(view: View) {

            isOrganization = (view as CheckBox).isChecked

            if (isOrganization == true) {
                linearLayoutCorporative.visibility = View.VISIBLE
            } else {
                linearLayoutCorporative.visibility = View.GONE
            }
        }

        internal fun createData() {

            //        Project project = new Project();
            //        project.setTitle(editTitle.getText().toString());
            //        project.setOwner(editOwner.getText().toString());
            //        project.setPlace(editPlace.getText().toString());
            //        project.setDescription(editDescription.getText().toString());
            //        if (editMinAge.getText() != null)
            //            project.setMinAge(Integer.parseInt(editMinAge.getText().toString()));
            //        project.setMinSchool(editMinSchool.getText().toString());
            //        project.setDateStart(editDateStart.getText().toString());
            //        project.setPeriod(editPeriod.getText().toString());
            //        project.setContactName(editContactName.getText().toString());
            //        project.setContactEmail(editContactEmail.getText().toString());
            //        project.setContactPhone(editContactPhone.getText().toString());
            //        project.setContactSite(editContactSite.getText().toString());
            //        project.setImageUrl(editImageURL.getText().toString());
            //
            //        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("projects");
            //        String key = databaseReference.push().getKey();
            //        databaseReference.child(key).setValue(project).addOnCompleteListener(new OnCompleteListener<Void>() {
            //            @Override
            //            public void onComplete(@NonNull Task<Void> task) {
            //                String success = task.isSuccessful() ? "Evento enviado! Aguarde a aprovação..." : "Ops! Ocorreu uma falha.";
            //                Toast.makeText(Create_Project.this, success, Toast.LENGTH_SHORT).show();
            //                Create_Project.this.finish();
            //            }
            //        });
        }
    }
