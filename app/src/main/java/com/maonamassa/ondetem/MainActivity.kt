package com.maonamassa.ondetem

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.crashlytics.android.Crashlytics

import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.maonamassa.ondetem.ListViewActivity
import com.maonamassa.ondetem.MySecondFragment
import com.maonamassa.ondetem.R

import com.maonamassa.ondetem.admin.LoginActivity

import com.maonamassa.ondetem.model.MapsFragment
import com.maonamassa.ondetem.model.Project
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_main.*

import java.util.ArrayList

import kotlinx.android.synthetic.main.fragment_list.*

class MainActivity : AppCompatActivity() {

    var mapFragment = MapsFragment()
    var listFragment = ListViewActivity()

    val filteredProjects = ArrayList<Project>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Fabric.with(this, Crashlytics())
        setupView()
        fetchData()
    }

    override fun onResume() {
        super.onResume()
        FirebaseAuth.getInstance().currentUser?.let {
            toLogin?.text = "LOG OUT"
        }
    }

    private fun setupView() {
        //To change body of created functions use File | Settings | File Templates.
        setSupportActionBar(myToolbar)
        myViewPager.adapter = MyViewPagerAdapter(this, supportFragmentManager)
        myTabLayout.setupWithViewPager(myViewPager)
        ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), 1)

        toLogin?.setOnClickListener {
            if (FirebaseAuth.getInstance().currentUser == null) {
                val intent = Intent(this, LoginActivity::class.java)
                this.startActivity(intent)
            } else {
                FirebaseAuth.getInstance().signOut()
                listFragment.hideProjectButton()
                toLogin?.text = "LOGIN"
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 1 && Build.VERSION.SDK_INT >= 23){
            mapFragment.requestLocation()
        }
    }

    private fun fetchData() {
            val databaseReference = FirebaseDatabase.getInstance().reference.child("projects")
            databaseReference.addValueEventListener(object: ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    filteredProjects.clear()
                    if (!dataSnapshot.exists()) {
                        return
                    }
                    val iterator = dataSnapshot.children.iterator()
                    val projects = ArrayList<Project>()
                    while (iterator.hasNext()) {
                        val project = iterator.next().getValue<Project>(Project::class.java)// cria novo objeto apartir de cada objeto do json
                        projects.add(project) // adiciona na lista de projetos
                    }

                    for (project in projects) {
                        if (false) { //TODO implementar junto com admin
                            filteredProjects.add(project)
                        } else {
                            if (project.isApproved) filteredProjects.add(project)
                        }
                    }
                    listFragment.filteredProjects = filteredProjects
                    mapFragment.updateList(filteredProjects)
                }

                override fun onCancelled(databaseError: DatabaseError) {}
            })
    }
}

class MyViewPagerAdapter(val context: Context, fragmentManager: FragmentManager?) : FragmentPagerAdapter(fragmentManager) {
    override fun getItem(position: Int): Fragment? {
        when(position){
            0 -> return (context as? MainActivity)?.listFragment
            1 -> return (context as? MainActivity)?.mapFragment
        }

        return null
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when(position){
            0 -> return context.getString(R.string.first)
            1 -> return context.getString(R.string.second)
        }

        return null
    }
}
