package com.maonamassa.ondetem

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.maonamassa.ondetem.adapter.EventsAdapter
import com.maonamassa.ondetem.admin.LoginActivity
import com.maonamassa.ondetem.model.Project
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.fragment_list.*
import java.util.*
import kotlin.collections.HashMap

class ListViewActivity : Fragment() {
    private var adapter: EventsAdapter? = null
    private var admin: Boolean = false
    private var mAuth: FirebaseAuth? = null

    var filteredProjects = ArrayList<Project>()
        set(list) {
            field = list
            if (adapter != null){
                adapter?.projects?.clear()
                adapter?.projects = list
                adapter?.notifyDataSetChanged()
            }
        }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_list, null, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = EventsAdapter()
        initList?.adapter = adapter

        if (filteredProjects.isNotEmpty()) {
            adapter?.projects?.clear()
            adapter?.projects = filteredProjects
            adapter?.notifyDataSetChanged()
        }

        //newProjectButton = findViewById(R.id.newProjectButton) as FloatingActionButton

        //fetchData()

        newProjectButton?.setOnClickListener {
            val intent = Intent(activity, Create_Project::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        newProjectButton?.visibility = View.GONE
        FirebaseAuth.getInstance().currentUser?.let {
            var loggedUser = FirebaseDatabase.getInstance().reference.child("users")
                    .child(it.uid)
            loggedUser.addListenerForSingleValueEvent(object: ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    var user = dataSnapshot.value as? HashMap<String, Any>
                    if (user?.get("isOrganization") as? Boolean ?: false && user?.get("isApproved") as? Boolean ?: false) {
                        newProjectButton?.visibility = View.VISIBLE
                    } else {
                        newProjectButton?.visibility = View.GONE
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {}
            })
        }
    }

    fun hideProjectButton() {
        newProjectButton?.visibility = View.GONE
    }
}

